source ~/.zshrc
sh pre-requisites.sh
if ! docker ps -a | grep "product-management" &> /dev/null
then
    echo "WARN: product-management image not found"
    cd backend
    sh build.sh
    cd ..
    echo "Please run again run.sh"
    exit 1
else
    echo "OK: docker is properly configured"
fi
if ! docker ps -a | grep "stock-management"> /dev/null
then
    echo "WARN: stock-management image not found"
    cd backend
    sh build.sh
    cd ..
    echo "Please run again run.sh"
    exit 1
else
    echo "OK: docker is properly configured"
fi
cd environment
docker-compose up