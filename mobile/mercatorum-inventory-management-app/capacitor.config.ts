import {CapacitorConfig} from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.mercatorum.rintilla.inventorymp',
  appName: 'Mercatorum Inventory Management',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
