import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePage} from './home.page';

const routes: Routes = [
  {
    path: 'home',
    component: HomePage,
    children: [
      {
        path: 'stock-items',
        loadChildren: () => import('../stock-items/stock-items.module').then(m => m.StockItemsPageModule)
      },
      {
        path: 'stock-movements',
        loadChildren: () => import('../stock-movements/stock-movements.module').then(m => m.StockMovementsPageModule)
      },
      {
        path: '',
        redirectTo: '/home/stock-items',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/home/stock-items',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class HomePageRoutingModule {
}
