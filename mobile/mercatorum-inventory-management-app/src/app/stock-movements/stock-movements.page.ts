import {Component, OnInit} from '@angular/core';
import {BarcodeScanner} from "@capacitor-mlkit/barcode-scanning";
import {AlertController} from "@ionic/angular";
import {Router} from "@angular/router";
import {ProductDTO, StockMovementDTO} from "../core/modules/openapi";
import {concatAll, filter, firstValueFrom, map, mergeAll, Observable, toArray} from "rxjs";
import {ProductsService} from "../core/modules/openapi";
import {StockItemModel} from "../core/models/stock-item.model";
import {StockMovementModel} from "../core/models/stock-movement.model";
import * as moment from 'moment';
import {StockMovementService} from "../core/modules/openapi";

@Component({
  selector: 'app-stock-movements',
  templateUrl: 'stock-movements.page.html',
  styleUrls: ['stock-movements.page.scss']
})
export class StockMovementsPage implements OnInit {

  searchBarFilter: string = "";
  isSupported?: boolean;
  stockMovements$: Observable<StockMovementModel[]> | undefined;
  products: ProductDTO[] = [];

  constructor(private alertController: AlertController, private router: Router, private stockMovementApi: StockMovementService, private productApi: ProductsService) {
  }


  async ngOnInit(): Promise<void> {
    try {
    const {supported} = await BarcodeScanner.isSupported();
    this.isSupported = supported;
    if (!supported)
      console.error("not supported");
    } catch (error) {

    }

    this.products = await firstValueFrom(this.productApi.getProducts());
    this.stockMovements$ = this.stockMovementApi.getStockMovements()
      .pipe(
        map(movements => movements.sort((m1, m2) => (m1.movementDateTime || 0) > (m2.movementDateTime || 0) ? -1 : 1)),
        concatAll(),
        map(movement => this.buildStockMovementModel(movement)),
        filter(movement => movement.barCode != null),
        toArray(),
      );
  }

  async scan(): Promise<void> {
    if(!this.isSupported) {
      await this.router.navigateByUrl(`/stock-scan/888462660341`)
    }
    const granted = await this.requestPermissions();
    if (!granted) {
      await this.presentAlert();
      return;
    }
    const {barcodes} = await BarcodeScanner.scan();

    if (barcodes.length > 0 && barcodes[0].displayValue) {
      await this.router.navigateByUrl(`/stock-scan/${barcodes[0].displayValue}`)
    }

  }

  async requestPermissions(): Promise<boolean> {
    const {camera} = await BarcodeScanner.requestPermissions();
    return camera === 'granted' || camera === 'limited';
  }

  async presentAlert(): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Permission denied',
      message: 'Please grant camera permission to use the barcode scanner.',
      buttons: ['OK'],
    });
    await alert.present();
  }

  private buildStockMovementModel(stockMovementDTO: StockMovementDTO): StockMovementModel {
    const product = this.products?.find(p => p.code === stockMovementDTO.productCode)
    return {
      productCode: stockMovementDTO.productCode,
      barCode: product?.barCode,
      picture: product?.picture,
      productName: product?.name,
      manufacturer: product?.manufacturer,
      productDescription: product?.description,
      movementQuantity: stockMovementDTO.quantity,
      movementType: stockMovementDTO.movementType,
      movementDateTime: moment(parseInt(String(stockMovementDTO.movementDateTime)?.substring(0, String(stockMovementDTO.movementDateTime).indexOf(".")) + "000")).calendar()
    } as StockMovementModel;
  }

  displayQty(stockMovement: StockMovementModel) {
    return (stockMovement.movementType === 'ON_SHELF' ? "+" : "-") + stockMovement.movementQuantity;
  }

  displayColor(stockMovement: StockMovementModel) {
    return stockMovement.movementType === 'ON_SHELF' ? "primary" : "success"
  }

  displayIcon(stockMovement: StockMovementModel) {
    return stockMovement.movementType === 'ON_SHELF' ? "arrow-forward" : "arrow-back"
  }
}
