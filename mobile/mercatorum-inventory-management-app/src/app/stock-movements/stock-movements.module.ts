import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {StockMovementsPage} from './stock-movements.page';
import {StockItemDetailsComponentModule} from '../stock-item-details/stock-item-details.module';

import {StockMovementsPageRoutingModule} from './stock-movements-routing.module';
import {PipesModule} from "../core/pipes/pipes.module";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    StockItemDetailsComponentModule,
    StockMovementsPageRoutingModule,
    PipesModule
  ],
  declarations: [StockMovementsPage]
})
export class StockMovementsPageModule {
}
