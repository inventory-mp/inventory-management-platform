import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StockMovementsPage} from './stock-movements.page';

const routes: Routes = [
  {
    path: '',
    component: StockMovementsPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockMovementsPageRoutingModule {
}
