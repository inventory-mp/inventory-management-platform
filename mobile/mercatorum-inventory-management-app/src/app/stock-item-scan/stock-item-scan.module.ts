import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {IonicModule} from "@ionic/angular";
import {StockItemScanPage} from "./stock-item-scan.component";
import {StockItemScanPageRoutingModule} from "./stock-item-scan-routing.module";

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, StockItemScanPageRoutingModule],
  declarations: [StockItemScanPage],
  exports: [StockItemScanPage]
})
export class StockItemScanComponentModule {

}
