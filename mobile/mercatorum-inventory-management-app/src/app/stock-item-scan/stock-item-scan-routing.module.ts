import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StockItemScanPage} from "./stock-item-scan.component";

const routes: Routes = [
  {
    path: ':barcode',
    component: StockItemScanPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockItemScanPageRoutingModule {
}
