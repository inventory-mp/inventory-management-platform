import {Component, OnInit, ViewChild} from '@angular/core';
import {Location} from "@angular/common";
import {ActivatedRoute, Router} from "@angular/router";
import {ProductDTO, ProductsService, StockItemDTO, StockMovementService, StockService} from "../core/modules/openapi";
import {firstValueFrom, zip} from "rxjs";
import {IonModal} from "@ionic/angular";

@Component({
  selector: 'app-stock-item-scan',
  templateUrl: './stock-item-scan.component.html',
  styleUrls: ['./stock-item-scan.component.scss'],
})
export class StockItemScanPage implements OnInit{

  @ViewChild(IonModal) modal: IonModal | undefined;
  quantity: number = 0;
  barCode: string;
  productDTO: ProductDTO | undefined;
  stockItemDTO: StockItemDTO | undefined;

  constructor(private location: Location, private route: ActivatedRoute, private router: Router, private stockItemApi: StockService, private productApi: ProductsService, private stockMovementApi: StockMovementService) {
    const barCode = this.route.snapshot.paramMap.get('barcode');
    if(!barCode)
      throw new Error("barCode is not provided");
    this.barCode = barCode;
  }

  async ngOnInit(): Promise<void> {
    const products = await firstValueFrom(this.productApi.getProducts(this.barCode));
    if(products.length < 1 || !products[0].code)
      throw new Error("Product does not exists");
    this.productDTO = products[0];

    this.stockItemDTO = await firstValueFrom(this.stockItemApi.getStockItem(this.productDTO.code || ""));
  }

  navigateBack(): void {
    this.location.back();
  }

  onWillDismiss($event: any): void {

  }

  cancel(): void {
    this.modal?.dismiss(null, 'cancel');
  }

  async confirm(): Promise<void> {
    this.modal?.dismiss(null, 'cancel');
    if(this.quantity != 0)
      await firstValueFrom(this.stockMovementApi.createStockMovement({
        quantity: this.quantity < 0 ? -(this.quantity) : this.quantity,
        movementType: this.quantity < 0 ? "ON_SELL" : "ON_SHELF",
        productCode: this.productDTO?.code
      }));

    await this.router.navigateByUrl(`/home`);
    window.location.reload()
  }

}
