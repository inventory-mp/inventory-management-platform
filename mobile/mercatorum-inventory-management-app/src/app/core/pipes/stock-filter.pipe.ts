import {Pipe, PipeTransform} from "@angular/core";
import {StockItemModel} from "../models/stock-item.model";
import {StockMovementModel} from "../models/stock-movement.model";

@Pipe({
  name: 'stockfilter'
})
export class StockFilterPipe implements PipeTransform {
  transform(items: StockItemModel[]|StockMovementModel[]|null, searchText: string): any[] {
    if (!items) {
      return [];
    }
    if(!searchText) {
      return items;
    }

    searchText = searchText.toLowerCase();

    return items.filter(item => {
      return item.productName?.toLowerCase().includes(searchText) || item.manufacturer?.toLowerCase().includes(searchText) || item.barCode?.toLowerCase().includes(searchText);
    });
  }
}
