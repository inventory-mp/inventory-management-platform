import {NgModule} from "@angular/core";
import {StockFilterPipe} from "./stock-filter.pipe";

@NgModule({
  declarations: [StockFilterPipe],
  exports: [StockFilterPipe]
})
export class PipesModule {

}
