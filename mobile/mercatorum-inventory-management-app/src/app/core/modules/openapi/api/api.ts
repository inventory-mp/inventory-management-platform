import { ProductsService } from "./products.service";
import { StockService } from './stock.service';
import { StockMovementService } from './stock-movement.service';

export * from './stock.service';
export * from './stock.serviceInterface';
export * from './stock-movement.service';
export * from './stock-movement.serviceInterface';
export * from './products.serviceInterface';
export * from './products.service';

export const APIS = [StockService, StockMovementService, ProductsService];
