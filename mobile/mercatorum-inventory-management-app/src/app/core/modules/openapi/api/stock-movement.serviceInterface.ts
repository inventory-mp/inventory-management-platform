/**
 * Products
 * API for managing products
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { HttpHeaders }                                       from '@angular/common/http';

import { Observable }                                        from 'rxjs';

import { CreateStockMovementDTO } from '../model/models';
import { StockMovementDTO } from '../model/models';


import { Configuration }                                     from '../configuration';



export interface StockMovementServiceInterface {
    defaultHeaders: HttpHeaders;
    configuration: Configuration;

    /**
     * Create stock movement
     * Create a stock movement
     * @param createStockMovementDTO Create Stock Movement Definition
     */
    createStockMovement(createStockMovementDTO: CreateStockMovementDTO, extraHttpRequestParams?: any): Observable<StockMovementDTO>;

    /**
     * Get stock movement
     * Get stock movements
     * @param productCode 
     */
    getStockMovements(productCode?: string, extraHttpRequestParams?: any): Observable<Array<StockMovementDTO>>;

}
