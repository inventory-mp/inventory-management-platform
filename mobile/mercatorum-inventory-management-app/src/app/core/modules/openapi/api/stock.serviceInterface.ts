/**
 * Products
 * API for managing products
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { HttpHeaders }                                       from '@angular/common/http';

import { Observable }                                        from 'rxjs';

import { StockItemDTO } from '../model/models';


import { Configuration }                                     from '../configuration';



export interface StockServiceInterface {
    defaultHeaders: HttpHeaders;
    configuration: Configuration;

    /**
     * Get stock item
     * Retrieve stock item by product
     * @param productCode Identifier of the product to get.
     */
    getStockItem(productCode: string, extraHttpRequestParams?: any): Observable<StockItemDTO>;

    /**
     * Get stock items
     * Retrieve a list of all stock items.
     */
    getStockItems(extraHttpRequestParams?: any): Observable<Array<StockItemDTO>>;

}
