import { InjectionToken } from '@angular/core';

export const BASE_PATH = new InjectionToken<string>('basePath');
export const PRODUCT_BASE_PATH = new InjectionToken<string>('productBasePath');
export const STOCK_BASE_PATH = new InjectionToken<string>('stockBasePath');
export const COLLECTION_FORMATS = {
    'csv': ',',
    'tsv': '   ',
    'ssv': ' ',
    'pipes': '|'
}
