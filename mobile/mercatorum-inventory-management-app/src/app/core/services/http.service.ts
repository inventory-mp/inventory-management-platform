import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {firstValueFrom, map} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient: HttpClient) {

  }

  get<T>(url: string, params?: { key: string, value: string }[], headers?: { key: string, value: string }[]): Promise<T> {

    const httpHeaders = new HttpHeaders();
    headers?.forEach(h => httpHeaders.set(h.key, h.value))

    const queryParams = new HttpParams();
    params?.forEach(p => queryParams.set(p.key, p.value))

    return firstValueFrom(this.httpClient.get(url, {
        params: queryParams,
        headers: httpHeaders,
        responseType: 'json'
      })
        .pipe(
          map(response => response as T)
        )
    );
  }

  post<T, S>(url: string, body: S, headers?: { key: string, value: string }[]): Promise<T> {

    const httpHeaders = new HttpHeaders();
    headers?.forEach(h => httpHeaders.set(h.key, h.value))

    return firstValueFrom(this.httpClient.post(url, body, {
        headers: httpHeaders,
        responseType: 'json',
      })
        .pipe(
          map(response => response as T)
        )
    );
  }

  put<T, S>(url: string, body: S, headers?: { key: string, value: string }[]): Promise<T> {

    const httpHeaders = new HttpHeaders();
    headers?.forEach(h => httpHeaders.set(h.key, h.value))

    return firstValueFrom(this.httpClient.put(url, body, {
        headers: httpHeaders,
        responseType: 'json',
      })
        .pipe(
          map(response => response as T)
        )
    );
  }

}
