export interface StockMovementModel {

  productCode?: string;
  onShelfQuantity?: number;
  manufacturer?: string;
  productName?: string;
  picture?: string;
  barCode?: string;
  productDescription?: string;
  movementDateTime?: string;
  movementType?: "ON_SHELF" | "ON_SELL";
  movementQuantity?: number;

}
