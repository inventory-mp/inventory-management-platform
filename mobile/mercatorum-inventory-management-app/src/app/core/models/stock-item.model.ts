export interface StockItemModel {

  productCode?: string;
  onShelfQuantity?: number;
  manufacturer?: string;
  productName?: string;
  picture?: string;
  barCode?: string;
  productDescription?: string;

}
