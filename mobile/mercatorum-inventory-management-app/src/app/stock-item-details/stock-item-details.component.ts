import {Component, OnInit} from '@angular/core';
import {Location} from "@angular/common";
import {ProductDTO, ProductsService, StockItemDTO, StockService} from "../core/modules/openapi";
import {ActivatedRoute} from "@angular/router";
import {firstValueFrom, toArray, zip} from "rxjs";

@Component({
  selector: 'app-stock-item-details',
  templateUrl: './stock-item-details.component.html',
  styleUrls: ['./stock-item-details.component.scss'],
})
export class StockItemDetailsPage implements OnInit {

  public productCode: string;
  public productDTO: ProductDTO | undefined;
  public stockItemDTO: StockItemDTO | undefined;
  public segmentType: string = "stock";

  constructor(private location: Location, private activatedRoute: ActivatedRoute, private stockItemApi: StockService, private productApi: ProductsService) {
    const productCode = this.activatedRoute.snapshot.paramMap.get('productCode');
    if (!productCode)
      throw new Error("productCode is not provided");

    this.productCode = productCode
  }

  async ngOnInit(): Promise<void> {
    const [stockDTO, productDTO] = await firstValueFrom(zip(
        this.stockItemApi.getStockItem(this.productCode),
        this.productApi.getProductByCode(this.productCode)
      )
    )

    this.stockItemDTO = stockDTO;
    this.productDTO = productDTO;
  }

  navigateBack(): void {
    this.location.back();
  }

}
