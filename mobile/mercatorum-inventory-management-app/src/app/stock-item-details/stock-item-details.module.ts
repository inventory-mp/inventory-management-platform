import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {StockItemDetailsPage} from './stock-item-details.component';
import {StockItemDetailsPageRoutingModule} from "./stock-item-details-routing.module";

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, StockItemDetailsPageRoutingModule],
  declarations: [StockItemDetailsPage],
  exports: [StockItemDetailsPage]
})
export class StockItemDetailsComponentModule {
}
