import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StockItemDetailsPage} from "./stock-item-details.component";

const routes: Routes = [
  {
    path: ':productCode',
    component: StockItemDetailsPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockItemDetailsPageRoutingModule {
}
