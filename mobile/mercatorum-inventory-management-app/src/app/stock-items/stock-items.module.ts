import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {StockItemsPage} from './stock-items.page';
import {StockItemDetailsComponentModule} from '../stock-item-details/stock-item-details.module';

import {StockItemsPageRoutingModule} from './stock-items-routing.module';
import {PipesModule} from "../core/pipes/pipes.module";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    StockItemDetailsComponentModule,
    StockItemsPageRoutingModule,
    PipesModule
  ],
  declarations: [StockItemsPage]
})
export class StockItemsPageModule {
}
