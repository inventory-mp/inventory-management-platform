import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ProductDTO, StockItemDTO, StockService} from "../core/modules/openapi";
import {filter, firstValueFrom, map, mergeAll, Observable, of, toArray} from "rxjs";
import {ProductsService} from "../core/modules/openapi/api/products.service";
import {StockItemModel} from "../core/models/stock-item.model";

@Component({
  selector: 'app-stock-items',
  templateUrl: 'stock-items.page.html',
  styleUrls: ['stock-items.page.scss']
})
export class StockItemsPage implements OnInit {

  stockItems$: Observable<StockItemModel[]> = of([]);
  products: ProductDTO[] | undefined;
  searchBarFilter: string;

  constructor(private router: Router, private stockItemsService: StockService, private productService: ProductsService) {
    this.searchBarFilter = "";
  }

  async ngOnInit(): Promise<void> {
    const stockItems = this.stockItemsService.getStockItems();
    this.products = await firstValueFrom(this.productService.getProducts());

    this.stockItems$ = stockItems.pipe(
      mergeAll(),
      map(item => this.buildStockItemModel(item)),
      filter(item => item.barCode != null),
      toArray()
    );
  }

  async goToDetails(productCode: string): Promise<boolean> {
    return await this.router.navigate(['home', 'stock-item-details', productCode]);
  }


  private buildStockItemModel(stockItemDTO: StockItemDTO): StockItemModel {
    const product = this.products?.find(p => p.code === stockItemDTO.productCode)
    return {
      productCode: stockItemDTO.productCode,
      onShelfQuantity: stockItemDTO.onShelfQuantity,
      barCode: product?.barCode,
      picture: product?.picture,
      productName: product?.name,
      manufacturer: product?.manufacturer,
      productDescription: product?.description
    } as StockItemModel;
  }


}
