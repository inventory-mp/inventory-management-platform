import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StockItemsPage} from './stock-items.page';

const routes: Routes = [
  {
    path: '',
    component: StockItemsPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockItemsPageRoutingModule {
}
