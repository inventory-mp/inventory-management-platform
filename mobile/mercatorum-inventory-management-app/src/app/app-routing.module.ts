import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'stock-items',
    loadChildren: () => import('./stock-item-details/stock-item-details.module').then(m => m.StockItemDetailsComponentModule)
  },
  {
    path: 'stock-scan',
    loadChildren: () => import('./stock-item-scan/stock-item-scan.module').then(m => m.StockItemScanComponentModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
