import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ApiModule, BASE_PATH, PRODUCT_BASE_PATH, STOCK_BASE_PATH} from "./core/modules/openapi";
import {HttpClientModule} from "@angular/common/http";
import {StockFilterPipe} from "./core/pipes/stock-filter.pipe";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, ApiModule],
  providers: [
    {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    { provide: PRODUCT_BASE_PATH, useValue: 'http://192.168.1.4:8001/api' },
    { provide: STOCK_BASE_PATH, useValue: 'http://192.168.1.4:8002/api' }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
