source ~/.zshrc
if ! command -v docker &> /dev/null
then
    echo "ERR: docker could not be found. Please install Docker!"
    exit 1
else
    echo "OK: docker is properly configured"
fi
if ! command -v mvn &> /dev/null
then
    echo "ERR: mvn command could not be found. Please configure Maven"
    exit 1
else
    echo "OK: maven is properly configured"
fi
if docker info > /dev/null 2>&1; then
    echo "OK: Docker is running."
else
    echo "ERR: Docker is not running. Please start Docker!"
    exit 1
fi
echo "Environment is ready to run Inventory MP!"
