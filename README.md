<h1 align="center">
   <a href="https://gitlab.com/inventory-mp/inventory-management-platform" target="_blank" align="center">
      Inventory Management Platform
   </a>
</h1>

<p align="center">An University Project - Developed by Roberto Intilla</p>

## Introduction 🚀

The system is proposed as a manager of basic operations that are carried out in small store warehouses.
In particular, it provides the ability to have visibility of the items in stock and any movement performed (entry or exit of an item from the warehouse), via a mobile app.
In order to perform entry or exit operations, the barcode of the item can be scanned via the app; the system will be able to recognize the item and allow the user to provide the quantity to be added or removed.

## Demo


![Alt Text](./assets/video.gif)

<img height="500" src="./assets/stockitems.png" />
<img height="500" src="./assets/stockmovements.png" />
<img height="500" src="./assets/itemscan.png" />


## Architecture Overview

<img height="500" src="./assets/IMP_Overview.jpg" />