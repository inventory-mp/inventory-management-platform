package com.mercatorum.rintilla.inventorymp.config;

@FunctionalInterface
public interface ConsumerConfigProvider {

    <T> void addConsumer(ConsumerDef<T> consumer);

}
