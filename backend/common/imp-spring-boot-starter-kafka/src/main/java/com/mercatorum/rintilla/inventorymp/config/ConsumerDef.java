package com.mercatorum.rintilla.inventorymp.config;

import com.mercatorum.rintilla.inventorymp.kafka.KafkaEventHandler;

public class ConsumerDef<T> {

    private final String topic;
    private final Class<T> messageTypeName;
    private final KafkaEventHandler<T> handler;

    public ConsumerDef(String topic, Class<T> messageTypeName, KafkaEventHandler<T> handler) {
        this.topic = topic;
        this.messageTypeName = messageTypeName;
        this.handler = handler;
    }

    public String getTopic() {
        return topic;
    }

    public Class<T> getMessageTypeName() {
        return messageTypeName;
    }

    public KafkaEventHandler<T> getHandler() {
        return handler;
    }
}
