package com.mercatorum.rintilla.inventorymp.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.*;

import java.time.Duration;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class DefaultConsumerConfigProvider implements ConsumerConfigProvider {

    private final KafkaConfigurationProperties kafkaConfigurationProperties;
    private final ObjectMapper objectMapper;

    DefaultConsumerConfigProvider(KafkaConfigurationProperties kafkaConfigurationProperties) {
        this.kafkaConfigurationProperties = kafkaConfigurationProperties;
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public <T> void addConsumer(ConsumerDef<T> consumerDef) {
        System.out.println("Registering consumer. Topic:=" + consumerDef.getTopic() + ", Handler:=" + consumerDef.getHandler());
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.execute(() -> {
            try {
                final Consumer<String, String> consumer = new KafkaConsumer<>(this.kafkaConfigurationProperties.asMap());
                consumer.subscribe(Collections.singletonList(consumerDef.getTopic()));
                while (true) {
                    ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(kafkaConfigurationProperties.getMaxPollIntervalMs()));
                    for (ConsumerRecord<String, String> record : records) {
                        String topic = record.topic();
                        String key = record.key();
                        String value = record.value();
                        System.out.printf("Consumed event from topic %s: key = %-10s value = %s%n", topic, key, value);
                        try {
                            var event = objectMapper.readValue(value, consumerDef.getMessageTypeName());
                            consumerDef.getHandler().handle(event)
                                    .block();
                            consumer.commitSync();
                        } catch (JsonProcessingException | CommitFailedException ex) {
                            System.out.printf("Exception occurred while processing an event on topic %s: %s%n", topic, ex.getMessage());
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.printf("Cannot create Kafka consumer due to: %s", ex.getMessage());
            }
        });
    }

}
