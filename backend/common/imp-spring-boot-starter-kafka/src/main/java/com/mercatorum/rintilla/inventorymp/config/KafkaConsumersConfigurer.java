package com.mercatorum.rintilla.inventorymp.config;

public interface KafkaConsumersConfigurer {

    void configure(ConsumerConfigProvider consumerConfigProvider);

}
