package com.mercatorum.rintilla.inventorymp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class KafkaAutoConfiguration {

    public KafkaAutoConfiguration(
            @Autowired(required = false) List<KafkaConsumersConfigurer> kafkaConfigurers,
            @Autowired KafkaConfigurationProperties kafkaConfigurationProperties) {

        DefaultConsumerConfigProvider defaultConsumerConfigProvider = new DefaultConsumerConfigProvider(kafkaConfigurationProperties);

        if (kafkaConfigurers != null) {
            for (KafkaConsumersConfigurer configurer : kafkaConfigurers) {
                configurer.configure(defaultConsumerConfigProvider);
            }
        }

    }

}
