package com.mercatorum.rintilla.inventorymp.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercatorum.rintilla.inventorymp.kafka.AsyncKafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(KafkaConfigurationProperties.class)
public class KafkaConfiguration {

    @Bean
    public AsyncKafkaProducer asyncKafkaProducer(@Autowired KafkaConfigurationProperties kafkaConfigurationProperties) {

        return new AsyncKafkaProducer(kafkaConfigurationProperties.asMap(), new ObjectMapper());
    }

}
