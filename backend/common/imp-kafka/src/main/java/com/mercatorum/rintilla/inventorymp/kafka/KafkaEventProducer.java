package com.mercatorum.rintilla.inventorymp.kafka;

public interface KafkaEventProducer<T> {

    void produce(T object);

}
