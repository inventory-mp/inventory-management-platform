package com.mercatorum.rintilla.inventorymp.kafka;

import reactor.core.publisher.Mono;

public interface KafkaEventHandler<T> {

    Mono<?> handle(T event);

}
