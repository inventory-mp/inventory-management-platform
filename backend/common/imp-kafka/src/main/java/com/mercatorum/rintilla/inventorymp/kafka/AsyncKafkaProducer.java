package com.mercatorum.rintilla.inventorymp.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Map;

public class AsyncKafkaProducer {
    private final ObjectMapper objectMapper;
    private final Map<String, Object> properties;

    public AsyncKafkaProducer(Map<String, Object> properties, ObjectMapper objectMapper) {
        this.properties = properties;
        this.objectMapper = objectMapper;
    }

    public <T> void produce(String topic, T item) {
        try (Producer<String, String> producer = new KafkaProducer<>(this.properties)) {
            producer.send(new ProducerRecord<>(topic, null, toJson(item)));
        }
    }

    private <T> String toJson(T item) {
        try {
            return objectMapper.writeValueAsString(item);
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

}
