package com.mercatorum.rintilla.inventorymp.mongo.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercatorum.rintilla.inventorymp.mongo.ReactiveMongoTemplate;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MongoDBConfiguration {

    @Value("${inventorymanagement.mongo.url}")
    private String url;
    @Value("${inventorymanagement.mongo.port}")
    private String port;
    @Value("${inventorymanagement.mongo.username}")
    private String username;
    @Value("${inventorymanagement.mongo.password}")
    private String password;
    @Value("${inventorymanagement.mongo.database}")
    private String database;

    @Bean
    public MongoDatabase createClient() {

        var connectionString = String.format("mongodb://%s:%s@%s:%s/%s", username, password, url, port, database);

        MongoClientSettings settings = MongoClientSettings.builder()
                .retryWrites(true)
                .applyConnectionString(new ConnectionString(connectionString))
                .build();

        return MongoClients.create(settings)
                .getDatabase(database);
    }

    @Bean
    public ReactiveMongoTemplate reactiveMongoTemplate(@Autowired MongoDatabase mongoDatabase) {
        return new ReactiveMongoTemplate(mongoDatabase, new ObjectMapper());
    }

}
