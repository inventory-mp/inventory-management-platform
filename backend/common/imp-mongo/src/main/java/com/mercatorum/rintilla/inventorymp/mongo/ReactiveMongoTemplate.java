package com.mercatorum.rintilla.inventorymp.mongo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.reactivestreams.client.FindPublisher;
import com.mongodb.reactivestreams.client.MongoDatabase;
import org.bson.Document;
import org.bson.json.JsonWriterSettings;
import org.bson.types.ObjectId;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class ReactiveMongoTemplate {

    private final MongoDatabase mongoDatabase;
    private final ObjectMapper objectMapper;
    private final JsonWriterSettings jsonWriterSettings;

    public ReactiveMongoTemplate(MongoDatabase database, ObjectMapper objectMapper) {
        this.mongoDatabase = database;
        this.objectMapper = objectMapper;

        jsonWriterSettings = JsonWriterSettings.builder()
                .objectIdConverter(((objectId, strictJsonWriter) -> {
                    strictJsonWriter.writeString(objectId.toString());
                }))
                .build();

    }

    public <T> Flux<T> find(String collection, Class<T> targetClass) {
        var findPublisher = mongoDatabase.getCollection(collection).find();
        return Flux.from(findPublisher)
                .map(document -> documentToObject(document, targetClass));
    }

    public <T> Mono<T> save(String collection, T object) {
        var insertOnePublisher = mongoDatabase.getCollection(collection).insertOne(objectToDocument(object));
        return Mono.from(insertOnePublisher)
                .map(res -> object);
    }

    public <T> Mono<T> update(String collection, String id, T object) {
        var replaceOnePublisher = mongoDatabase.getCollection(collection).replaceOne(new Document("_id", new ObjectId(id)), objectToDocument(object));
        return Mono.from(replaceOnePublisher)
                .map(res -> object);
    }

    public <T> Flux<T> findBy(String collection, Class<T> targetClass, EntityFilter filter) {
        FindPublisher<Document> findByPublisher;
        if(filter.getParameters().size() > 0)
            findByPublisher = mongoDatabase.getCollection(collection).find(new BasicDBObject(filter.getParameters()));
        else
            findByPublisher = mongoDatabase.getCollection(collection).find();
        return Flux.from(findByPublisher)
                .map(document -> documentToObject(document, targetClass));
    }

    private <T> T documentToObject(Document document, Class<T> targetClass) {
        try {
            document.put("id", document.getObjectId("_id").toString());
            return objectMapper.readValue(document.toJson(jsonWriterSettings), targetClass);
        } catch (JsonProcessingException ex) {
            throw new RuntimeException(ex);
        }
    }

    private <T> Document objectToDocument(T object) {
        try {
            var json = objectMapper.writeValueAsString(object);
            return Document.parse(json);
        } catch (JsonProcessingException ex) {
            throw new RuntimeException(ex);
        }
    }


}
