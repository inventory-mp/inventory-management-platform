package com.mercatorum.rintilla.inventorymp.mongo;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class EntityFilter {

    private final Map<String, Object> parameters = new HashMap<>();

    public void putParameter(String key, Object value) {
        this.parameters.put(key, value);
    }

    public Map<String, Object> getParameters() {
        return this.parameters.entrySet().stream()
                .filter(entrySet -> entrySet.getValue() != null)
                .collect(Collectors.toMap((Map.Entry::getKey), (Map.Entry::getValue)));
    }
}
