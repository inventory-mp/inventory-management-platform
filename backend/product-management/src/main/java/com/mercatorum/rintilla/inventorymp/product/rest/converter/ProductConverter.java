package com.mercatorum.rintilla.inventorymp.product.rest.converter;

import com.mercatorum.rintilla.inventorymp.openapi.CreateProductDTO;
import com.mercatorum.rintilla.inventorymp.product.model.CreateProduct;
import com.mercatorum.rintilla.inventorymp.product.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductConverter {

    ProductConverter INSTANCE = Mappers.getMapper(ProductConverter.class);

    com.mercatorum.rintilla.inventorymp.openapi.ProductDTO fromModel(Product model);

    CreateProduct toModel(CreateProductDTO dto);

}
