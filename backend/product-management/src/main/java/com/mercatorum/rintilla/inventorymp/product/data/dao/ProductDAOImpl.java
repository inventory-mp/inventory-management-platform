package com.mercatorum.rintilla.inventorymp.product.data.dao;

import com.mercatorum.rintilla.inventorymp.entity.ProductEntity;
import com.mercatorum.rintilla.inventorymp.mongo.ReactiveMongoTemplate;
import com.mercatorum.rintilla.inventorymp.product.data.filter.ProductFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class ProductDAOImpl implements ProductDAO {

    private static final String COLLECTION_NAME = "products";

    @Autowired
    private ReactiveMongoTemplate reactiveMongoTemplate;

    @Override
    public Flux<ProductEntity> findAll() {
        return reactiveMongoTemplate.find(COLLECTION_NAME, ProductEntity.class);
    }

    @Override
    public Mono<ProductEntity> save(ProductEntity product) {
        return reactiveMongoTemplate.save(COLLECTION_NAME, product);
    }

    @Override
    public Flux<ProductEntity> findBy(ProductFilter productFilter) {
        return reactiveMongoTemplate.findBy(COLLECTION_NAME, ProductEntity.class, productFilter);
    }


}
