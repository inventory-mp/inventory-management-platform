package com.mercatorum.rintilla.inventorymp.product.data.converter;

import com.mercatorum.rintilla.inventorymp.entity.ProductEntity;
import com.mercatorum.rintilla.inventorymp.product.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductConverter {

    ProductConverter INSTANCE = Mappers.getMapper(ProductConverter.class);

    Product fromEntity(ProductEntity productEntity);

    ProductEntity toEntity(Product product);



}
