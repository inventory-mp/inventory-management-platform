package com.mercatorum.rintilla.inventorymp.product.event.producer;

import com.mercatorum.rintilla.inventorymp.event.ProductCreatedUpdated;
import com.mercatorum.rintilla.inventorymp.kafka.AsyncKafkaProducer;
import com.mercatorum.rintilla.inventorymp.kafka.KafkaEventProducer;
import com.mercatorum.rintilla.inventorymp.product.event.converter.ProductConverter;
import com.mercatorum.rintilla.inventorymp.product.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ProductCreatedUpdatedProducer implements KafkaEventProducer<Product> {

    @Autowired
    private AsyncKafkaProducer producer;
    @Value("${inventorymanagement.kafka.topics.producer.product-created-updated}")
    private String destinationTopic;

    @Override
    public void produce(Product product) {
        ProductCreatedUpdated eventObject = ProductConverter.INSTANCE.fromModel(product);
        producer.produce(destinationTopic, eventObject);
    }

}
