package com.mercatorum.rintilla.inventorymp.product.event.converter;

import com.mercatorum.rintilla.inventorymp.event.ProductCreatedUpdated;
import com.mercatorum.rintilla.inventorymp.product.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductConverter {

    ProductConverter INSTANCE = Mappers.getMapper(ProductConverter.class);

    ProductCreatedUpdated fromModel(Product model);

}
