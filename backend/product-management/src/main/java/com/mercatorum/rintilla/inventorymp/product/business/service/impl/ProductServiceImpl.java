package com.mercatorum.rintilla.inventorymp.product.business.service.impl;

import com.mercatorum.rintilla.inventorymp.product.business.service.ProductService;
import com.mercatorum.rintilla.inventorymp.product.data.converter.ProductConverter;
import com.mercatorum.rintilla.inventorymp.product.data.dao.ProductDAO;
import com.mercatorum.rintilla.inventorymp.product.data.filter.ProductFilter;
import com.mercatorum.rintilla.inventorymp.product.event.producer.ProductCreatedUpdatedProducer;
import com.mercatorum.rintilla.inventorymp.product.model.CreateProduct;
import com.mercatorum.rintilla.inventorymp.product.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private ProductCreatedUpdatedProducer productCreatedUpdatedProducer;

    @Override
    public Flux<Product> findBy(ProductFilter filter) {
        return productDAO.findBy(filter)
                .map(ProductConverter.INSTANCE::fromEntity);
    }

    @Override
    public Flux<Product> findAll() {
        return productDAO.findAll()
                .map(ProductConverter.INSTANCE::fromEntity);
    }

    @Override
    public Mono<Product> create(CreateProduct createProduct) {
        var createProduct$ = Mono.just(createProduct)
                .map(ProductServiceImpl::buildProductFromCreateProduct)
                .map(ProductConverter.INSTANCE::toEntity)
                .flatMap(productDAO::save)
                .map(ProductConverter.INSTANCE::fromEntity)
                .doOnNext(model -> productCreatedUpdatedProducer.produce(model));

        return findBy(new ProductFilter().withBarCode(createProduct.getBarCode()))
                .next()
                .<Product>flatMap(p -> Mono.error(new Exception("Product already exists")))
                .switchIfEmpty(createProduct$);

    }

    public static Product buildProductFromCreateProduct(CreateProduct createProduct) {
        return Product.builder()
                .code(UUID.randomUUID().toString())
                .name(createProduct.getName())
                .description(createProduct.getDescription())
                .manufacturer(createProduct.getManufacturer())
                .barCode(createProduct.getBarCode())
                .picture(createProduct.getPicture())
                .build();
    }

}
