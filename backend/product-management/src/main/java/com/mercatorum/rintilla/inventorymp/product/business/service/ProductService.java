package com.mercatorum.rintilla.inventorymp.product.business.service;

import com.mercatorum.rintilla.inventorymp.product.data.filter.ProductFilter;
import com.mercatorum.rintilla.inventorymp.product.model.CreateProduct;
import com.mercatorum.rintilla.inventorymp.product.model.Product;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProductService {

    Flux<Product> findBy(ProductFilter productFilter);

    Flux<Product> findAll();

    Mono<Product> create(CreateProduct product);

}
