package com.mercatorum.rintilla.inventorymp.product.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CreateProduct {

    private final String name;
    private final String description;
    private final String manufacturer;
    private final String barCode;
    private final String picture;


}
