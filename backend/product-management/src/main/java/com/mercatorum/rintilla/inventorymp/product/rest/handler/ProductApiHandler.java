package com.mercatorum.rintilla.inventorymp.product.rest.handler;

import com.mercatorum.rintilla.inventorymp.openapi.CreateProductDTO;
import com.mercatorum.rintilla.inventorymp.openapi.ProductDTO;
import com.mercatorum.rintilla.inventorymp.openapi.ProductsApi;
import com.mercatorum.rintilla.inventorymp.product.business.service.ProductService;
import com.mercatorum.rintilla.inventorymp.product.data.filter.ProductFilter;
import com.mercatorum.rintilla.inventorymp.product.rest.converter.ProductConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class ProductApiHandler implements ProductsApi {

    @Autowired
    private ProductService productService;

    @Override
    public Mono<ResponseEntity<Flux<ProductDTO>>> getProducts(String barCode, ServerWebExchange exchange) {
        var productFlux = productService.findBy(new ProductFilter().withBarCode(barCode))
                .map(ProductConverter.INSTANCE::fromModel);

        return Mono.just(ResponseEntity.ok(productFlux));
    }

    @Override
    public Mono<ResponseEntity<ProductDTO>> createProduct(Mono<CreateProductDTO> createProductDTO, ServerWebExchange exchange) {
        return createProductDTO
                .map(ProductConverter.INSTANCE::toModel)
                .flatMap(model -> productService.create(model))
                .map(ProductConverter.INSTANCE::fromModel)
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<ProductDTO>> getProductByCode(String productCode, ServerWebExchange exchange) {
        return productService.findBy(new ProductFilter().withProductCode(productCode))
                .next()
                .map(ProductConverter.INSTANCE::fromModel)
                .map(ResponseEntity::ok);
    }

}
