package com.mercatorum.rintilla.inventorymp.product.data.dao;

import com.mercatorum.rintilla.inventorymp.entity.ProductEntity;
import com.mercatorum.rintilla.inventorymp.product.data.filter.ProductFilter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProductDAO {

    Flux<ProductEntity> findAll();

    Mono<ProductEntity> save(ProductEntity product);

    Flux<ProductEntity> findBy(ProductFilter productFilter);

}
