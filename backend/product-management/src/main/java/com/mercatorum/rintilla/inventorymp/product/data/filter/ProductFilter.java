package com.mercatorum.rintilla.inventorymp.product.data.filter;

import com.mercatorum.rintilla.inventorymp.mongo.EntityFilter;

public class ProductFilter extends EntityFilter {

    public ProductFilter() {

    }

    public ProductFilter(String productCode, String barCode) {
        this.withProductCode(barCode);
        this.withBarCode(barCode);
    }

    public ProductFilter withBarCode(String barCode) {
        super.putParameter("barCode", barCode);
        return this;
    }

    public ProductFilter withProductCode(String productCode) {
        super.putParameter("code", productCode);
        return this;
    }

}
