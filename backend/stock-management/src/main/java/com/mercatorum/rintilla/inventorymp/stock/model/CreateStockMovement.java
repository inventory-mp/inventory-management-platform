package com.mercatorum.rintilla.inventorymp.stock.model;

import com.mercatorum.rintilla.inventorymp.stock.model.enums.MovementTypeEnum;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CreateStockMovement {

    private final String productCode;
    private final MovementTypeEnum movementType;
    private final Integer quantity;

}
