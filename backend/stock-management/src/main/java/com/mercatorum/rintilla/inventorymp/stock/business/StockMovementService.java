package com.mercatorum.rintilla.inventorymp.stock.business;

import com.mercatorum.rintilla.inventorymp.stock.model.CreateStockMovement;
import com.mercatorum.rintilla.inventorymp.stock.model.StockMovement;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface StockMovementService {

    Flux<StockMovement> findBy(String productCode);

    Mono<StockMovement> create(CreateStockMovement createStockMovement);

}
