package com.mercatorum.rintilla.inventorymp.stock.event.consumer;

import com.mercatorum.rintilla.inventorymp.event.StockMovementCreated;
import com.mercatorum.rintilla.inventorymp.kafka.KafkaEventHandler;
import com.mercatorum.rintilla.inventorymp.stock.business.StockItemService;
import com.mercatorum.rintilla.inventorymp.stock.model.CreateUpdateStockItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class StockMovementCreatedHandler implements KafkaEventHandler<StockMovementCreated>  {

    @Autowired
    private StockItemService stockItemService;

    @Override
    public Mono<?> handle(StockMovementCreated event) {
        return stockItemService.upsert(buildCreateUpdateStockItem(event));
    }

    private static CreateUpdateStockItem buildCreateUpdateStockItem(StockMovementCreated stockMovementCreated) {
        var onShelf = stockMovementCreated.getMovementType().equals(StockMovementCreated.MovementType.ON_SHELF) ? stockMovementCreated.getQuantity() : -stockMovementCreated.getQuantity();

        return CreateUpdateStockItem.builder()
                .productCode(stockMovementCreated.getProductCode())
                .onShelfQuantity(onShelf)
                .upsertType(CreateUpdateStockItem.UpsertType.CALCULATE)
                .build();
    }

}
