package com.mercatorum.rintilla.inventorymp.stock.rest.handler;

import com.mercatorum.rintilla.inventorymp.openapi.StockApi;
import com.mercatorum.rintilla.inventorymp.openapi.StockItemDTO;
import com.mercatorum.rintilla.inventorymp.stock.business.StockItemService;
import com.mercatorum.rintilla.inventorymp.stock.rest.converter.StockItemConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class StockApiHandler implements StockApi {

    @Autowired
    private StockItemService stockItemService;

    @Override
    public Mono<ResponseEntity<Flux<StockItemDTO>>> getStockItems(ServerWebExchange exchange) {
        var flux = stockItemService.findBy(null)
                .map(StockItemConverter.INSTANCE::fromModel);

        return Mono.just(ResponseEntity.ok(flux));
    }

    @Override
    public Mono<ResponseEntity<StockItemDTO>> getStockItem(String productCode, ServerWebExchange exchange) {
        return stockItemService.findBy(productCode)
                .next()
                .map(StockItemConverter.INSTANCE::fromModel)
                .map(ResponseEntity::ok);
    }


}
