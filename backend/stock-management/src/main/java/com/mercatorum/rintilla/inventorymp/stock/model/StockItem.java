package com.mercatorum.rintilla.inventorymp.stock.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class StockItem {


    private final String productCode;
    private final Integer onShelfQuantity;

}
