package com.mercatorum.rintilla.inventorymp.stock.model.enums;

public enum MovementTypeEnum {

    ON_SHELF("ON_SHELF"),
    ON_SELL("ON_SELL");

    private final String type;

    MovementTypeEnum(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return this.type;
    }
}
