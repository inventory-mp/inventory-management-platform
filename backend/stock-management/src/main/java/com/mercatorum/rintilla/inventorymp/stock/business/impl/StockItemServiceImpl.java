package com.mercatorum.rintilla.inventorymp.stock.business.impl;

import com.mercatorum.rintilla.inventorymp.entity.StockItemEntity;
import com.mercatorum.rintilla.inventorymp.stock.business.StockItemService;
import com.mercatorum.rintilla.inventorymp.stock.data.converter.StockItemConverter;
import com.mercatorum.rintilla.inventorymp.stock.data.dao.StockItemDAO;
import com.mercatorum.rintilla.inventorymp.stock.data.filter.StockItemFilter;
import com.mercatorum.rintilla.inventorymp.stock.model.CreateUpdateStockItem;
import com.mercatorum.rintilla.inventorymp.stock.model.StockItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class StockItemServiceImpl implements StockItemService {

    @Autowired
    private StockItemDAO stockItemDAO;

    @Override
    public Flux<StockItem> findBy(String productCode) {
        return stockItemDAO.findBy(new StockItemFilter(productCode))
                .map(StockItemConverter.INSTANCE::fromEntity);
    }

    @Override
    public Mono<StockItem> upsert(CreateUpdateStockItem createUpdateStockItem) {
        return Flux.just(createUpdateStockItem)
                .flatMap(cusitem -> stockItemDAO.findBy(new StockItemFilter(cusitem.getProductCode())))
                .next()
                .defaultIfEmpty(new StockItemEntity())
                .flatMap(currentStockItem ->
                        Mono.just(createUpdateStockItem)
                                .map(input -> StockItemServiceImpl.buildStockItem(currentStockItem, input))
                                .map(StockItemConverter.INSTANCE::toEntity)
                                .flatMap(stockItem -> createOrUpdate(currentStockItem.getId(), stockItem))
                                .map(StockItemConverter.INSTANCE::fromEntity)
                );
    }

    private Mono<StockItemEntity> createOrUpdate(String id, StockItemEntity entity) {
        if(id == null)
            return stockItemDAO.insert(entity);
        else
            return stockItemDAO.update(id, entity);
    }

    private static StockItem buildStockItem(StockItemEntity oldStockItem, CreateUpdateStockItem createUpdateStockItem) {
        return StockItem.builder()
                .productCode(createUpdateStockItem.getProductCode())
                .onShelfQuantity(calculateOnShelf(oldStockItem.getOnShelfQuantity(), createUpdateStockItem))
                .build();
    }

    private static Integer calculateOnShelf(Integer currentOnShelf, CreateUpdateStockItem createUpdateStockItem) {
        if(createUpdateStockItem.getUpsertType() != null && createUpdateStockItem.getUpsertType().equals(CreateUpdateStockItem.UpsertType.CALCULATE)) {
            return (currentOnShelf != null ? currentOnShelf : 0) + createUpdateStockItem.getOnShelfQuantity();
        } else {
            return createUpdateStockItem.getOnShelfQuantity();
        }
    }

}
