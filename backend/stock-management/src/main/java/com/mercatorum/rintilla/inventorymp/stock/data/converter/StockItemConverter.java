package com.mercatorum.rintilla.inventorymp.stock.data.converter;

import com.mercatorum.rintilla.inventorymp.entity.StockItemEntity;
import com.mercatorum.rintilla.inventorymp.stock.model.StockItem;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StockItemConverter {

    StockItemConverter INSTANCE = Mappers.getMapper(StockItemConverter.class);

    StockItemEntity toEntity(StockItem stockItem);

    StockItem fromEntity(StockItemEntity stockItem);

}
