package com.mercatorum.rintilla.inventorymp.stock.event.config;

import com.mercatorum.rintilla.inventorymp.config.ConsumerConfigProvider;
import com.mercatorum.rintilla.inventorymp.config.ConsumerDef;
import com.mercatorum.rintilla.inventorymp.config.KafkaConsumersConfigurer;
import com.mercatorum.rintilla.inventorymp.event.StockMovementCreated;
import com.mercatorum.rintilla.inventorymp.stock.event.consumer.StockMovementCreatedHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaConfigurer implements KafkaConsumersConfigurer {

    @Autowired
    private StockMovementCreatedHandler stockMovementCreatedHandler;

    @Override
    public void configure(ConsumerConfigProvider consumerConfigProvider) {
        consumerConfigProvider.addConsumer(new ConsumerDef<>("stock-movements", StockMovementCreated.class, stockMovementCreatedHandler));
    }

}
