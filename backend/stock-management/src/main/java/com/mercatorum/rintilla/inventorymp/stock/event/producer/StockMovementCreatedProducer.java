package com.mercatorum.rintilla.inventorymp.stock.event.producer;

import com.mercatorum.rintilla.inventorymp.event.StockMovementCreated;
import com.mercatorum.rintilla.inventorymp.kafka.AsyncKafkaProducer;
import com.mercatorum.rintilla.inventorymp.kafka.KafkaEventProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class StockMovementCreatedProducer implements KafkaEventProducer<StockMovementCreated> {

    @Autowired
    private AsyncKafkaProducer producer;
    @Value("${inventorymanagement.kafka.topics.producer.stock-movement-created}")
    private String destinationTopic;

    @Override
    public void produce(StockMovementCreated object) {
        producer.produce(destinationTopic, object);
    }

}
