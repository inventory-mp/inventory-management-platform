package com.mercatorum.rintilla.inventorymp.stock.model;

import com.mercatorum.rintilla.inventorymp.stock.model.enums.MovementTypeEnum;
import lombok.Builder;
import lombok.Getter;

import java.time.ZonedDateTime;

@Getter
@Builder
public class StockMovement {

    private final MovementTypeEnum movementType;
    private final String productCode;
    private final Integer quantity;
    private final ZonedDateTime movementDateTime;

}
