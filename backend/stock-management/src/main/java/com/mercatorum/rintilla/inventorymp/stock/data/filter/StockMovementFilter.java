package com.mercatorum.rintilla.inventorymp.stock.data.filter;

import com.mercatorum.rintilla.inventorymp.mongo.EntityFilter;

public class StockMovementFilter extends EntityFilter {

    public StockMovementFilter(String productCode) {
        setProductCode(productCode);
    }

    public void setProductCode(String productCode) {
        putParameter("productCode", productCode);
    }

}
