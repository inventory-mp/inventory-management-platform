package com.mercatorum.rintilla.inventorymp.stock.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CreateUpdateStockItem {

    public enum UpsertType {
        REPLACE,
        CALCULATE
    }

    private final String productCode;
    private final Integer onShelfQuantity;
    private final UpsertType upsertType;

}
