package com.mercatorum.rintilla.inventorymp.stock.event.producer;

import com.mercatorum.rintilla.inventorymp.event.StockItemCreatedUpdated;
import com.mercatorum.rintilla.inventorymp.kafka.AsyncKafkaProducer;
import com.mercatorum.rintilla.inventorymp.kafka.KafkaEventProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class StockItemCreatedUpdatedProducer implements KafkaEventProducer<StockItemCreatedUpdated> {

    @Autowired
    private AsyncKafkaProducer producer;
    @Value("${inventorymanagement.kafka.topics.producer.stock-item-created-updated}")
    private String destinationTopic;

    @Override
    public void produce(StockItemCreatedUpdated object) {
        producer.produce(destinationTopic, object);
    }

}
