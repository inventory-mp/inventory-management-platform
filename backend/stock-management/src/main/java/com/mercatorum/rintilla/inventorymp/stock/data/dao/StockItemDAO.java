package com.mercatorum.rintilla.inventorymp.stock.data.dao;

import com.mercatorum.rintilla.inventorymp.entity.StockItemEntity;
import com.mercatorum.rintilla.inventorymp.stock.data.filter.StockItemFilter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface StockItemDAO {

    Flux<StockItemEntity> findBy(StockItemFilter stockItemFilter);

    Mono<StockItemEntity> update(String id, StockItemEntity stockItemEntity);

    Mono<StockItemEntity> insert(StockItemEntity stockItemEntity);

}
