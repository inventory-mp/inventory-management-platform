package com.mercatorum.rintilla.inventorymp.stock.rest.handler;

import com.mercatorum.rintilla.inventorymp.openapi.CreateStockMovementDTO;
import com.mercatorum.rintilla.inventorymp.openapi.StockMovementApi;
import com.mercatorum.rintilla.inventorymp.openapi.StockMovementDTO;
import com.mercatorum.rintilla.inventorymp.stock.business.StockMovementService;
import com.mercatorum.rintilla.inventorymp.stock.rest.converter.StockMovementConverter;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
public class StockMovementApiHandler implements StockMovementApi {

    @Autowired
    private StockMovementService stockMovementService;

    @Override
    public Mono<ResponseEntity<Flux<StockMovementDTO>>> getStockMovements(String productCode, ServerWebExchange exchange) {
        var stockMovementsFlux = stockMovementService.findBy(productCode)
                .map(StockMovementConverter.INSTANCE::fromModel);

        return Mono.just(ResponseEntity.ok(stockMovementsFlux));
    }

    @Override
    public Mono<ResponseEntity<StockMovementDTO>> createStockMovement(Mono<CreateStockMovementDTO> createStockMovementDTO, ServerWebExchange exchange) {
        return createStockMovementDTO
                .map(StockMovementConverter.INSTANCE::toModel)
                .flatMap(model -> stockMovementService.create(model))
                .map(StockMovementConverter.INSTANCE::fromModel)
                .map(ResponseEntity::ok);
    }


}
