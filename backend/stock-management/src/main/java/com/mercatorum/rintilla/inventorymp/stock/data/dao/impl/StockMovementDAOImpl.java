package com.mercatorum.rintilla.inventorymp.stock.data.dao.impl;

import com.mercatorum.rintilla.inventorymp.entity.StockMovementEntity;
import com.mercatorum.rintilla.inventorymp.mongo.ReactiveMongoTemplate;
import com.mercatorum.rintilla.inventorymp.stock.data.dao.StockMovementDAO;
import com.mercatorum.rintilla.inventorymp.stock.data.filter.StockMovementFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class StockMovementDAOImpl implements StockMovementDAO {

    private static final String COLLECTION_NAME = "stock-movements";

    @Autowired
    private ReactiveMongoTemplate reactiveMongoTemplate;

    @Override
    public Flux<StockMovementEntity> findBy(StockMovementFilter stockMovementFilter) {
        return reactiveMongoTemplate.findBy(COLLECTION_NAME, StockMovementEntity.class, stockMovementFilter);
    }

    @Override
    public Mono<StockMovementEntity> create(StockMovementEntity stockMovementEntity) {
        return reactiveMongoTemplate.save(COLLECTION_NAME, stockMovementEntity);
    }

}
