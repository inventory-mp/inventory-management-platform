package com.mercatorum.rintilla.inventorymp.stock.data.converter;

import com.mercatorum.rintilla.inventorymp.entity.StockMovementEntity;
import com.mercatorum.rintilla.inventorymp.stock.model.StockMovement;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StockMovementConverter {

    StockMovementConverter INSTANCE = Mappers.getMapper(StockMovementConverter.class);

    StockMovementEntity toEntity(StockMovement stockItemEntity);

    StockMovement fromEntity(StockMovementEntity stockItem);

}
