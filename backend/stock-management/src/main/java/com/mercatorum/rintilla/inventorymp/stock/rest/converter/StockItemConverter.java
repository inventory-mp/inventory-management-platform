package com.mercatorum.rintilla.inventorymp.stock.rest.converter;

import com.mercatorum.rintilla.inventorymp.openapi.StockItemDTO;
import com.mercatorum.rintilla.inventorymp.stock.model.StockItem;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StockItemConverter {

    StockItemConverter INSTANCE = Mappers.getMapper(StockItemConverter.class);

    StockItemDTO fromModel(StockItem stockItem);


}
