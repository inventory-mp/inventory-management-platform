package com.mercatorum.rintilla.inventorymp.stock.event.converter;

import com.mercatorum.rintilla.inventorymp.event.StockMovementCreated;
import com.mercatorum.rintilla.inventorymp.stock.model.StockMovement;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StockMovementCreatedConverter {

    static final StockMovementCreatedConverter INSTANCE = Mappers.getMapper(StockMovementCreatedConverter.class);

    StockMovement convert(StockMovementCreated stockMovementCreated);

    StockMovementCreated convert(StockMovement stockMovementCreated);

}
