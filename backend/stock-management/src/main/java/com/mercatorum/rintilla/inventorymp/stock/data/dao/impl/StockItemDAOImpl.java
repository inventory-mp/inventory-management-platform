package com.mercatorum.rintilla.inventorymp.stock.data.dao.impl;

import com.mercatorum.rintilla.inventorymp.entity.StockItemEntity;
import com.mercatorum.rintilla.inventorymp.mongo.ReactiveMongoTemplate;
import com.mercatorum.rintilla.inventorymp.stock.data.dao.StockItemDAO;
import com.mercatorum.rintilla.inventorymp.stock.data.filter.StockItemFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class StockItemDAOImpl implements StockItemDAO {

    private static final String COLLECTION_NAME = "stock-items";

    @Autowired
    private ReactiveMongoTemplate reactiveMongoTemplate;

    @Override
    public Flux<StockItemEntity> findBy(StockItemFilter stockItemFilter) {
        return reactiveMongoTemplate.findBy(COLLECTION_NAME, StockItemEntity.class, stockItemFilter);
    }

    @Override
    public Mono<StockItemEntity> update(String id, StockItemEntity newEntity) {
        return reactiveMongoTemplate.update(COLLECTION_NAME, id, newEntity);
    }

    @Override
    public Mono<StockItemEntity> insert(StockItemEntity stockItemEntity) {
        return reactiveMongoTemplate.save(COLLECTION_NAME, stockItemEntity);
    }

}
