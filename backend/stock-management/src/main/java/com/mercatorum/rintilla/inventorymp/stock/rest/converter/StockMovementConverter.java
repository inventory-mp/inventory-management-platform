package com.mercatorum.rintilla.inventorymp.stock.rest.converter;

import com.mercatorum.rintilla.inventorymp.openapi.CreateStockMovementDTO;
import com.mercatorum.rintilla.inventorymp.openapi.StockMovementDTO;
import com.mercatorum.rintilla.inventorymp.stock.model.CreateStockMovement;
import com.mercatorum.rintilla.inventorymp.stock.model.StockMovement;
import com.mercatorum.rintilla.inventorymp.stock.model.enums.MovementTypeEnum;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.OffsetDateTime;
import java.time.ZonedDateTime;

@Mapper
public interface StockMovementConverter {

    StockMovementConverter INSTANCE = Mappers.getMapper(StockMovementConverter.class);

    @Mapping(source = "movementType", target = "movementType", qualifiedByName = "toMovementType")
    StockMovementDTO fromModel(StockMovement stockMovement);

    @Mapping(source = "movementType", target = "movementType", qualifiedByName = "fromMovementType")
    CreateStockMovement toModel(CreateStockMovementDTO createStockMovement);

    @Named("toMovementType")
    default StockMovementDTO.MovementTypeEnum toMovementType(MovementTypeEnum movementType) {
        return StockMovementDTO.MovementTypeEnum.fromValue(movementType.toString());
    }

    @Named("fromMovementType")
    default MovementTypeEnum map(CreateStockMovementDTO.MovementTypeEnum value) {
        return MovementTypeEnum.valueOf(value.toString());
    }

    default OffsetDateTime convert(ZonedDateTime dateTime) {
        return dateTime.toOffsetDateTime();
    }

    default ZonedDateTime convert(OffsetDateTime dateTime) {
        return dateTime.toZonedDateTime();
    }

}
