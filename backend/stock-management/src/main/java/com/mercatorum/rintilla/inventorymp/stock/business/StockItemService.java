package com.mercatorum.rintilla.inventorymp.stock.business;

import com.mercatorum.rintilla.inventorymp.stock.model.CreateUpdateStockItem;
import com.mercatorum.rintilla.inventorymp.stock.model.StockItem;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface StockItemService {

    Flux<StockItem> findBy(String productCode);

    Mono<StockItem> upsert(CreateUpdateStockItem createUpdateStockItem);

}
