package com.mercatorum.rintilla.inventorymp.stock.data.dao;

import com.mercatorum.rintilla.inventorymp.entity.StockMovementEntity;
import com.mercatorum.rintilla.inventorymp.stock.data.filter.StockMovementFilter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface StockMovementDAO {

    Flux<StockMovementEntity> findBy(StockMovementFilter stockMovementFilter);

    Mono<StockMovementEntity> create(StockMovementEntity stockMovementEntity);

}
