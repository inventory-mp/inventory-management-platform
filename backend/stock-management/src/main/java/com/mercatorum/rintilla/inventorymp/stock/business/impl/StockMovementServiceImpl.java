package com.mercatorum.rintilla.inventorymp.stock.business.impl;

import com.mercatorum.rintilla.inventorymp.kafka.AsyncKafkaProducer;
import com.mercatorum.rintilla.inventorymp.kafka.KafkaEventProducer;
import com.mercatorum.rintilla.inventorymp.stock.business.StockMovementService;
import com.mercatorum.rintilla.inventorymp.stock.data.converter.StockMovementConverter;
import com.mercatorum.rintilla.inventorymp.stock.data.dao.StockMovementDAO;
import com.mercatorum.rintilla.inventorymp.stock.data.filter.StockMovementFilter;
import com.mercatorum.rintilla.inventorymp.stock.event.converter.StockMovementCreatedConverter;
import com.mercatorum.rintilla.inventorymp.stock.event.producer.StockMovementCreatedProducer;
import com.mercatorum.rintilla.inventorymp.stock.model.CreateStockMovement;
import com.mercatorum.rintilla.inventorymp.stock.model.StockMovement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@Service
public class StockMovementServiceImpl implements StockMovementService {

    @Autowired
    private StockMovementDAO stockMovementDAO;
    @Autowired
    private StockMovementCreatedProducer stockMovementCreatedProducer;

    @Override
    public Flux<StockMovement> findBy(String productCode) {
        return stockMovementDAO.findBy(new StockMovementFilter(productCode))
                .map(StockMovementConverter.INSTANCE::fromEntity);
    }

    @Override
    public Mono<StockMovement> create(CreateStockMovement createStockMovement) {
        return Mono.just(createStockMovement)
                .map(StockMovementServiceImpl::buildStockMovement)
                .map(StockMovementConverter.INSTANCE::toEntity)
                .flatMap(entity -> stockMovementDAO.create(entity))
                .map(StockMovementConverter.INSTANCE::fromEntity)
                .doOnNext(this::notifyStockMovementCreated);
    }

    private void notifyStockMovementCreated(StockMovement stockMovement) {
        var stockMovementCreated = StockMovementCreatedConverter.INSTANCE.convert(stockMovement);
        stockMovementCreatedProducer.produce(stockMovementCreated);
    }

    private static StockMovement buildStockMovement(CreateStockMovement createStockMovement) {
        return StockMovement.builder()
                .productCode(createStockMovement.getProductCode())
                .movementType(createStockMovement.getMovementType())
                .quantity(createStockMovement.getQuantity())
                .movementDateTime(ZonedDateTime.now(ZoneOffset.UTC))
                .build();
    }

}
