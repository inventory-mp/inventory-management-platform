package com.mercatorum.rintilla.inventorymp.stock.data.filter;

import com.mercatorum.rintilla.inventorymp.mongo.EntityFilter;

public class StockItemFilter extends EntityFilter {

    public StockItemFilter(String productCode) {
        putParameter("productCode", productCode);
    }

    public void setProductCode(String productCode) {
        super.putParameter("productCode", productCode);
    }


}
