package com.mercatorum.rintilla.inventorymp.mongoinitializr;

import com.google.gson.Gson;
import com.mongodb.*;
import com.mongodb.client.MongoDatabase;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class Main {

    public static void main(String... args) throws URISyntaxException, IOException {
        String configFilePath = System.getenv("CONFIG_FILE_PATH");
        System.out.printf("Reading configuration file provided in %s %n", configFilePath);

        var resourcePath = Paths.get(configFilePath);
        var json = new String(Files.readAllBytes(resourcePath));

        var mongoUri = System.getenv("MONGO_URI");
        var mongoPort = System.getenv("MONGO_PORT");
        var mongoUser = System.getenv("MONGO_ADMIN_USER");
        var mongoPsw = System.getenv("MONGO_ADMIN_PSW");
        var mongoDb = System.getenv("MONGO_ADMIN_DB");

        var serverAddress = new ServerAddress(mongoUri, Integer.parseInt(mongoPort));
        var mongoCredentials = MongoCredential.createCredential(mongoUser, mongoDb, mongoPsw.toCharArray());

        System.out.printf("Connecting to MongoDB... %n");


        try (var mongo = new MongoClient(serverAddress, mongoCredentials, new MongoClientOptions.Builder().build())) {

            System.out.printf("Connecting established! %n");

            var gson = new Gson();
            var settings = gson.fromJson(json, Settings.class);


            for (Settings.Database database : settings.getDatabases()) {
                System.out.printf("[%s] Database setup%n", database.getName());
                var db = mongo.getDatabase(database.getName());

                for (Settings.User user : database.getUsers()) {
                    createUser(db, user);
                }

                for (Settings.Collection collection : database.getCollections()) {
                    createCollection(db, collection);
                }

            }
        }

    }

    private static void createUser(MongoDatabase db, Settings.User user) {
        try {
            System.out.printf("[%s] Creating user...%n", user.getUsername());

            var roles = user.getRoles().stream()
                    .map(role -> new BasicDBObject("role", role)
                            .append("db", db.getName())
                    )
                    .collect(Collectors.toList());

            final BasicDBObject createUserCommand = new BasicDBObject("createUser", user.getUsername())
                    .append("pwd", user.getPassword())
                    .append("roles", roles);
            db.runCommand(createUserCommand);
        } catch (MongoCommandException ex) {
            System.err.printf("%s", ex.getMessage());
        }
    }

    private static void createCollection(MongoDatabase db, Settings.Collection collection) {
        try {
            System.out.printf("[%s] Creating collection...%n", collection.getName());
            db.createCollection(collection.getName());
            System.out.printf("[%s] Collection created!%n", collection.getName());
        } catch (MongoCommandException ex) {
            System.err.printf("%s", ex.getMessage());
        }
    }

}
