source ~/.zshrc
cd common
sh mvn-build.sh
cd ..
cd mongo-initializr
sh mvn-build.sh
cd ..
cd product-management
sh mvn-build.sh
sh docker-build.sh
cd ..
cd stock-management
sh mvn-build.sh
sh docker-build.sh